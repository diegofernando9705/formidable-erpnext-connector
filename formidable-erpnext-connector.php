<?php
/**
 * Plugin Name: Formidable ERPNext Connector
 * Plugin URI: http://www.fianzacredito.com/formidable-erpnext-connector
 * Description: Connection between Formidable and ERPNext.
 * Version: 1.0
 * Author: Fianzacrédito
 * Author URI: http://www.fianzacredito.com
 */

add_action('frm_after_create_entry', 'sync_to_erpnext', 30, 2);
add_action("admin_menu", "plugin_erp");



if(isset($_POST['action']) && $_POST['action'] == "nuevoregistro"){
        update_option('id_formulario', $_POST['id_form']);
        update_option('clave_api',$_POST['clave_api']);
        update_option('secret_api',$_POST['clave_secret']);
		update_option('url_api',$_POST['url_api']);
    }

function sync_to_erpnext($entry_id, $form_id) {

    if ($form_id == get_option('id_formulario')) {
		
        $data = array(
            'lead_name' => '',
            'company_name' => '',
            'email_id' => '',
			'contact_by' => 'sistemas@fianzacredito.com',
            'source' => 'Web Form',
			'contact_date' => '',
            'phone' => '',
			'territory' => ''
        );
	
		
        if (isset($_POST['item_meta'][124])) {
            $data['lead_name'] = $_POST['item_meta'][124];
        }

        if (isset($_POST['item_meta'][125])) {
            $data['company_name'] = $_POST['item_meta'][125];
        }

        if (isset($_POST['item_meta'][126])) {
            $data['territory'] = $_POST['item_meta'][126];
        }

        if (isset($_POST['item_meta'][127])) {
            $data['email_id'] = $_POST['item_meta'][127];
        }
		
		$data['contact_by'] = "sistemas@fianzacredito.com";

        if (isset($_POST['item_meta'][128])) {
            $data['phone'] = $_POST['item_meta'][128];
			
		}
		
		date_default_timezone_set('America/Bogota');
		
		$formato_fecha = getdate(); //Funcion capturas Fecha y hora actual
		$dia_hoy = $formato_fecha['mday']; //Explode donde se captura el dia actual
		$dia_manana = $dia_hoy + 1; //Se suma un dia, para capturar el dia de mañana
		
		//Validacion para verificar que no sobrepase los dias del mes
		if($dia_manana == date( 't' ) OR $dia_manana < date( 't' )){
			$fecha = $formato_fecha['year']."-".$formato_fecha['mon']."-".$dia_manana." 08:00:00";
		}else{
			$fecha = $formato_fecha['year']."-".$formato_fecha['mon']."-01"." 08:00:00";
		}
		
		$data['contact_date'] = $fecha;
		
		$clave_api = get_option('clave_api');
		$clave_secret = get_option('secret_api');
		
        $response = wp_remote_request(get_option('url_api'), array(
            'method' => 'POST',
            'headers' => array(
                'Authorization' => 'token '.$clave_api.':'.$clave_secret
            ),
            'body' => json_encode($data),
        ));

        $code = wp_remote_retrieve_response_code($response);

        if ($code != 200) {
            throw new Exception("Error al sincronizar con la API");
        }
    }
}



function plugin_erp() {
  add_menu_page('ERP Connector', 'ERP Connector', 'manage_options', 'menu_config_api', 'api_key');
}


function api_key() { ?>
    

    <style>
		body{
			font-family:'Arial';	
		}
	
	
	.logo{
		background-color:white !important;
		border:1px solid grey;
		width:98%;
		padding:0px !important;
		margin-top:15px !important;
		margin-right:125px !important;
		
	}
	
	.logo img{
		width:150px;
		margin:5px 5px 5px 15px !important;
	}
	
	.envio{
		cursor:pointer;
		text-transform: uppercase;
    letter-spacing: px;
    padding: 12px 24px;
    border-radius: 50px;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    background: #00a65a;
    border: 1px solid #00a65a;
		color:white;
	}
		
		.titulo{
			padding-right:100px;
			font-weight:bold;
		}
		
		.table_config{
			width:95% !important;
		}
		
		tr{
			display: table-row;
    vertical-align: inherit;
    border-color: inherit;
		}
		
		label{
			
		}
</style>

<div class="logo">
	<img src='https://fianzacredito.com/wp-content/uploads/2019/07/logo-fianzacredito.png'>
</div>

<div class="formulario">
	
		<p style="font-size:18px;">
			Por lo general, no es necesario modificar esta configuración. Por favor, ten cuidado si cambias algo aquí.
		</p>

	<hr>
	<br>
	<form method="post">
		<input type="hidden" name="action" value="nuevoregistro">
		
		<table class="table_config">
			<tr style="margin-bottom:20px;">
				<td style="width:30%;" style="font-size:16px;">
					<label ><b>URL API</b></label>
				</td>
				<td>
					<input type="text" name="url_api" value='<?=get_option('url_api')?>'>
					<br>
					Introduce el parámetro url de la API del entorno de ERPNext que debes activar en este sitio.<br>					<br></td>
			</tr>
			<tr>
				<td style="font-size:16px;"><b>ID Formulario Formidable</b></td>
				<td><input type="text" name="id_form" value='<?=get_option('id_formulario')?>'>
				<br>
				Introduce el parámetro ID Formulario. Esta información es suministrada por el Plugin 'Formidable'.<br><br>
				</td>
			</tr>
			<tr>
				<td style="font-size:16px;"><b>Clave API</b></td>
				<td><input type="text" name="clave_api" value="<?=get_option('clave_api')?>">
				<br>
				La clave del API se encuentra en el ERPNext, en el Apartado de API Access del usuario con el que desea trabajar.<br><br></td>
			</tr>
			<tr>
				<td style="font-size:16px;"><b>Clave secreta API</b></td>
				<td><input type="password" name="clave_secret" value="<?=get_option('secret_api')?>"><br>
				La clave secreta del API se encuentra en el ERPNext, en el Apartado de API Access del usuario con el que desea trabajar.<br><br>
				</td>
			</tr>
		</table>	
		<br><br><br>	
		<center>
			<button type="submit" class="envio" style="margint-top:100px;">
				REGISTRAR
			</button>
		</center>
	</form>
</div>

<?php } ?>
