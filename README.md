# Formidable ERPNext Connector

Se realiza plugin con formulario, donde se permite ingresar el API Access y la URL del Api de ERPNext


### Los siguientes campos se encontraran en el Formulario
~~~
1. URL Api: Se colocará como bien dice el nombre, la URL de la API del Frappe
2. API Clave: Es arrojada desde el Frappe
3. API Access: Es arrojada desde el Frappe
4. Formulario ID: Es el id del formulario del Formidable, los puedes ver en las opciones de este plugin.
~~~